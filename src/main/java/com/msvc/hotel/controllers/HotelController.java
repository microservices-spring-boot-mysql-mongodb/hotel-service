package com.msvc.hotel.controllers;

import com.msvc.hotel.entity.Hotel;
import com.msvc.hotel.services.HotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/hoteles")
public class HotelController {

    @Autowired
    private HotelService hotelService;

    @GetMapping
    public ResponseEntity<List<Hotel>> getAll(){
        return  ResponseEntity.ok(hotelService.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Hotel> get(@PathVariable String id){
        return ResponseEntity.ok(hotelService.get(id));
    }

    @PostMapping
    public ResponseEntity<Hotel> guardar(@RequestBody Hotel hotel){
        String id = UUID.randomUUID().toString();
        hotel.setId(id);
        Hotel hs = hotelService.create(hotel);
        return  ResponseEntity.status(HttpStatus.CREATED).body(hs);
    }


}
